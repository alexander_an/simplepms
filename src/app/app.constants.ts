export const EntityTypes = {
  PRODUCT: "Product",
  CATEGORY: "Category"
};

export const EditableTypes = {
  NAME: "Name",
  DESCRIPTION: "Description"
}