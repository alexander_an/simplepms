import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SharedModule } from './../shared/shared.module';
import { CoreRoutingModule } from './core-routing.module';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProductListComponent } from './products/product-list/product-list.component';
import { CategoryListComponent } from './categories/category-list/category-list.component';
import { CategoryService } from '../shared/services/category.service';
import { ProductService } from '../shared/services/product.service';
import { CategoryComponent } from './categories/category/category.component';
import { AddCategoryComponent } from './categories/add-category/add-category.component';
import { ProductComponent } from './products/product/product.component';
import { AddProductComponent } from './products/add-product/add-product.component';
import { CategoryProductsComponent } from './categories/category-products/category-products.component';
import { CategoryProductsToggleComponent } from './categories/category-products-toggle/category-products-toggle.component';
@NgModule({
  declarations:
    [
      HeaderComponent,
      FooterComponent,
      HomeComponent,
      ProductListComponent,
      ProductComponent,
      AddProductComponent,
      CategoryListComponent,
      CategoryComponent,
      AddCategoryComponent,
      CategoryProductsComponent,
      CategoryProductsToggleComponent],
  imports: [
    SharedModule,
    CoreRoutingModule,
  ],
  exports: [
    RouterModule,
    HeaderComponent,
    FooterComponent
  ],
  providers:
    [
      CategoryService,
      ProductService
    ],
  bootstrap: [AddCategoryComponent, AddProductComponent]
})
export class CoreModule { }
