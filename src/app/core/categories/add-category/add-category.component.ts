import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Category } from 'src/app/shared/models/category';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css'],
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class AddCategoryComponent {
  public category = new Category(0, "","");

  constructor(private modalService:NgbActiveModal) { 

  }

  save(){
      this.modalService.close(this.category);
  }

  close(){
    this.modalService.close();
  }

  dismiss(){
    this.modalService.dismiss();
  }
}
