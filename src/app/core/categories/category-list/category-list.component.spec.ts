import { CategoryListComponent } from "./category-list.component";
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { CategoryComponent } from '../category/category.component';
import { CategoryProductsToggleComponent } from '../category-products-toggle/category-products-toggle.component';
import { CategoryProductsComponent } from '../category-products/category-products.component';
import { CategoryService } from 'src/app/shared/services/category.service';
import { By } from '@angular/platform-browser';
import { PagerComponent } from 'src/app/shared/pager/pager.component';
describe('CategoryListComponent', () => {
let component: CategoryListComponent;
  let fixture: ComponentFixture<CategoryListComponent>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CategoryListComponent, CategoryComponent, CategoryProductsToggleComponent, CategoryProductsComponent, PagerComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryListComponent);
    component = fixture.debugElement.componentInstance;
  });
  it("should create Quote component", () => {
    expect(component).toBeTruthy();
  });

  it("should fetch data",() => {
    const categoryService = fixture.debugElement.injector.get(CategoryService);
    let expected:any;
    categoryService.all().subscribe(res => expected = res);
    let spy = spyOn(categoryService, "all").and.callThrough();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
        let result:any;
        component.categories$.subscribe(res => result = res);
        expect(result).toBe(expected);
    });
  });

  it("should show product data",() => {
    fixture.detectChanges();
    fixture.debugElement
        .query(By.css('app-category-products-toggle'))
        .triggerEventHandler("click", null);
        fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.innerHTML).toContain("Test Product");
  });
});