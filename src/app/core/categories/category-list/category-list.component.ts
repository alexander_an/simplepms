import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { CategoryService } from 'src/app/shared/services/category.service';
import { Category } from 'src/app/shared/models/category';
import { isNumber } from 'util';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddCategoryComponent } from '../add-category/add-category.component';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoryListComponent implements OnInit {
  public categories$: any;
  public name: string;
  public description: string;
  pagedCategories: any[];

  constructor(private modalService: NgbModal,
     private categoryService: CategoryService, 
     private cdr: ChangeDetectorRef) {
  }

  add() {
    const modalRef = this.modalService.open(AddCategoryComponent);
    modalRef.result.then(res => {
      if (res !== undefined) {
        this.categoryService.add(res);
      }
    });
  }

  update(category: Category) {
    this.categoryService.update(category);
  }

  delete(categoryId: number) {
    if (isNumber(categoryId * 1)) {
      this.categoryService.delete(categoryId).subscribe();
    }
  }

  ngOnInit() {
    this.categories$ = this.categoryService.all();
  }

  displayPagedItems(items)
  {
    this.pagedCategories = items;
    this.cdr.detectChanges();
  }
}
