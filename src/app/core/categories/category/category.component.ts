import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditableTypes, EntityTypes } from 'src/app/app.constants';
import { Category } from 'src/app/shared/models/category';
import { ConfirmDeleteComponent } from 'src/app/shared/confirm-delete/confirm-delete.component';
import { UpdateEntityComponent } from 'src/app/shared/update-entity/update-entity.component';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoryComponent implements OnInit {
  @Input() category: Category;
  @Output() delete = new EventEmitter<any>();
  @Output() update = new EventEmitter<Category>();
  private entityType = EntityTypes.CATEGORY;
  public editableTypes = EditableTypes;
  public categoryProductsIsOpened = false;

  constructor(private modalService: NgbModal, private cdr: ChangeDetectorRef) {

  }

  refresh() {
    this.cdr.detectChanges();
  }

  ngOnInit() {
  }

  onDelete() {
    const modalRef = this.modalService.open(ConfirmDeleteComponent);
    modalRef.componentInstance.name = this.category.name;
    modalRef.componentInstance.entityType = this.entityType;
    modalRef.result.then(c => {
      if ((/true/i).test(c) === true) {
        this.delete.emit(this.category.id.toString());
      }
    });
  }

  onUpdate(type: string) {
    const updateModal = this.modalService.open(UpdateEntityComponent);
    if (type == EditableTypes.NAME) {
      updateModal.componentInstance.entity = this.category.name;
    }
    else {
      updateModal.componentInstance.entity = this.category.description;
    }
    updateModal.componentInstance.entityType = this.entityType;
    updateModal.componentInstance.type = type;
    updateModal.result.then(res => {
      if (res !== undefined) {
        if (type == EditableTypes.NAME) {
          this.category.name = res;
        }
        else {
          this.category.description = res;
        }
        this.update.emit(this.category);
      }
    })
  }

  toggleProducts() {
    this.categoryProductsIsOpened = !this.categoryProductsIsOpened;
  }

}
