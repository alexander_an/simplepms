import { Component, OnInit, Input, HostBinding, ChangeDetectionStrategy } from '@angular/core';
import { ProductService } from 'src/app/shared/services/product.service';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Product } from 'src/app/shared/models/product';

@Component({
  selector: 'app-category-products',
  templateUrl: './category-products.component.html',
  styleUrls: ['./category-products.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoryProductsComponent implements OnInit {
  @Input() categoryId: number;
  @HostBinding('class.is-open') @Input()
  isOpen = false;
  products$: Observable<Product[]>;
  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.products$ = this.productService.allByCategoryId(this.categoryId);
  }

}
