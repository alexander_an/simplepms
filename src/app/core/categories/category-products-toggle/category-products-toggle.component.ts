import { Component, OnInit, Output, EventEmitter, HostListener, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-category-products-toggle',
  templateUrl: './category-products-toggle.component.html',
  styleUrls: ['./category-products-toggle.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoryProductsToggleComponent {
 @Output() toggle: EventEmitter<null> = new EventEmitter();
  
 @HostListener('click')

 click(){
   this.toggle.emit();
 }
}
