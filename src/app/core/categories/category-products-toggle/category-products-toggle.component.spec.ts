import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryProductsToggleComponent } from './category-products-toggle.component';

describe('CategoryProductsToggleComponent', () => {
  let component: CategoryProductsToggleComponent;
  let fixture: ComponentFixture<CategoryProductsToggleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryProductsToggleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryProductsToggleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
