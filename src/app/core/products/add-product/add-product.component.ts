import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Category } from 'src/app/shared/models/category';
import { Product } from 'src/app/shared/models/product';
import { CategoryService } from 'src/app/shared/services/category.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddProductComponent implements OnInit {
  public product: Product = new Product(0, "", "", new Date());
  public categories$: Observable<Category[]>;
  constructor(private modalService: NgbActiveModal, private categoryService: CategoryService) { }
  save() {
    this.modalService.close(this.product);
  }
  close() {
    this.modalService.close();
  }
  dismiss() {
    this.modalService.dismiss();
  }
  ngOnInit() {
    this.categories$ = this.categoryService.all();
  }
}
