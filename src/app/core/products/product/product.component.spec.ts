import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProductComponent } from './product.component';
import { Product } from 'src/app/shared/models/product';
import { NgModel } from '@angular/forms';

describe('ProductComponent', () => {
  let component: ProductComponent;
  let fixture: ComponentFixture<ProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductComponent, NgModel ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductComponent);
    component = fixture.componentInstance;
    component.product = new Product(1, "Test Product", "Test Product Description", new Date());
    fixture.detectChanges();
  });

  it('should create', () => {
    console.log(component.product);
    expect(component).toBeTruthy();
  });
});
