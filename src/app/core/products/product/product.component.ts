import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { Product } from 'src/app/shared/models/product';
import { EntityTypes, EditableTypes } from 'src/app/app.constants';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmDeleteComponent } from 'src/app/shared/confirm-delete/confirm-delete.component';
import { UpdateEntityComponent } from 'src/app/shared/update-entity/update-entity.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductComponent implements OnInit {

  @Input() product: Product;
  @Output() delete = new EventEmitter<any>();
  @Output() update = new EventEmitter<Product>();
  private entityType = EntityTypes.PRODUCT;
  public editableTypes = EditableTypes;
  constructor(private modalService: NgbModal, private cdr: ChangeDetectorRef) {

  }

  refresh() {
    this.cdr.detectChanges();
  }

  ngOnInit() {
  }

  onDelete() {
    const modalRef = this.modalService.open(ConfirmDeleteComponent);
    console.log(this.product);
    modalRef.componentInstance.name = this.product.name;
    modalRef.componentInstance.entityType = this.entityType;
    modalRef.result.then(c => {
      if ((/true/i).test(c) === true) {
        this.delete.emit(this.product.id.toString());
      }
    });
  }

  onUpdate(type: string) {
    const updateModal = this.modalService.open(UpdateEntityComponent);
    if (type == EditableTypes.NAME) {
      updateModal.componentInstance.entity = this.product.name;
    }
    else {
      updateModal.componentInstance.entity = this.product.description;
    }
    updateModal.componentInstance.entityType = this.entityType;
    updateModal.componentInstance.type = type;
    updateModal.result.then(res => {
      if (res !== undefined) {
        if (type == EditableTypes.NAME) {
          console.log(this.product);
          this.product.name = res;
        }
        else {
          this.product.description = res;
        }
        this.update.emit(this.product);
      }
    })
  }

}
