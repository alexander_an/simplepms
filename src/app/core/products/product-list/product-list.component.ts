import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Product } from 'src/app/shared/models/product';
import { EntityTypes } from 'src/app/app.constants';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProductService } from 'src/app/shared/services/product.service';
import { AddProductComponent } from '../add-product/add-product.component';
import { isNumber } from 'util';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductListComponent implements OnInit {
  public products$: Observable<Product[]>;
  public name: string;
  public description: string;
  public pagedProducts: any[];
  constructor(private modalService: NgbModal, private productService: ProductService, private cdr: ChangeDetectorRef) {
  }
  add() {
    const modalRef = this.modalService.open(AddProductComponent);
    modalRef.result.then(res => {
      if (res !== undefined) {
        this.productService.add(res);
      }
    });
  }

  update(product: Product) {
    this.productService.update(product);
  }

  delete(productId: number) {
    if (isNumber(productId * 1)) {
      this.productService.delete(productId);
      
    }
  }

  ngOnInit() {
    this.products$ = this.productService.all();
  }

  displayPagedItems(items)
  {
    this.pagedProducts = items;
    this.cdr.detectChanges();
  }
}
