import { Component, ChangeDetectionStrategy, Input, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EntityTypes } from 'src/app/app.constants';

@Component({
  selector: 'app-confirm-delete',
  templateUrl: './confirm-delete.component.html',
  styleUrls: ['./confirm-delete.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmDeleteComponent {
  @Input() name;
  @Input() entityType;
  constructor(private modalService: NgbActiveModal) { }
  close(res) {
    this.modalService.close(res);
  }
  dismiss(res) {
    this.modalService.dismiss(res);
  }
}
