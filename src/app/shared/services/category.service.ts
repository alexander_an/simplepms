import { Injectable } from '@angular/core';
import { Category } from '../models/category';
import { Observable, Subscriber, of, from, BehaviorSubject } from 'rxjs'
@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  public categories$: BehaviorSubject<Category[]>;
  private categories: Category[] = [];
  constructor() {
    this.categories.push(new Category(1, "First Category", "First Category Description"));
    this.categories$ = new BehaviorSubject<Category[]>(this.categories);
  }

  all(): Observable<Category[]> {
    return this.categories$.asObservable();
  }

  byId(categoryId: number): Observable<Category> {
    var category = this.categories.find(c => c.id == categoryId);
    return Observable.create(observer => {
      observer.next(category);
      observer.complete;
    });
  }

  add(category: Category): Observable<number> {
    if (this.categories === undefined || this.categories.length === 0) {
      category.id = 1;
    }
    else {
      category.id = this.categories[this.categories.length - 1].id + 1;
    }
    this.categories.push(category);
    this.categories$.next(this.categories);

    return Observable.create(observer => {
      observer.next(category.id);
      observer.complete;
    });
  }

  delete(categoryId: number): Observable<boolean> {
    let result: boolean = true;
    var dbCategory = this.categories.find(c => c.id == categoryId);
    if (dbCategory === undefined || dbCategory === null) {
      result = false;
    }
    else {
      this.categories.splice(this.categories.indexOf(dbCategory), 1);
    }
    this.categories$.next(this.categories);
    return Observable.create(observer => {
      observer.next(result);
      observer.complete;
    });
  }


  update(category: Category): Observable<boolean> {
    let result: boolean = true;
    var dbCategory = this.categories.find(c => c.id == category.id);
    if (dbCategory === undefined || dbCategory === null) {
      result = false;
    }
    else {
      dbCategory.description = category.description;
      dbCategory.name = category.name;
    }
    this.categories$.next(this.categories);
    return Observable.create(observer => {
      observer.next(result);
      observer.complete;
    });
  }
}
