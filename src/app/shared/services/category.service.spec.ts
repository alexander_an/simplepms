import { TestBed } from '@angular/core/testing';

import { CategoryService } from './category.service';
import { Category } from '../models/category';

describe('CategoryService', () => {
  let service:CategoryService;
  let testCategory = new Category(0, "Test", "Test Description");
  beforeEach(() => {
    service = new CategoryService();
  });

  it('should be created', () => {
    const service: CategoryService = TestBed.get(CategoryService);
    expect(service).toBeTruthy();
  });

  it('should create new category', () =>{
    let result = [];
    service.add(testCategory).subscribe(res=> testCategory.id = res);
    expect(testCategory.id).toBe(2);
    service.all().subscribe(res => result = res);
    expect(result.length).toBe(2);
  });

  it('should remove the category',() =>{
    let result = false;
    service.add(testCategory).subscribe(res=> testCategory.id = res);
    service.delete(testCategory.id).subscribe(res => result = res);
    expect(result).toBe(true);
  });

  it('should update the category',() =>{
    let result:any;
    service.add(testCategory).subscribe(res=> testCategory.id = res);
    testCategory.name = 'Test1';
    service.update(testCategory);
    service.byId(testCategory.id).subscribe(res => result = res);
    expect(result.name).toBe('Test1');
  });
});