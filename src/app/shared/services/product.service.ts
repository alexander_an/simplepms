import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import { BehaviorSubject, Observable } from 'rxjs';
import { Category } from '../models/category';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  public products$: BehaviorSubject<Product[]>;
  private products: Product[] = [];
  constructor() {
    let product = new Product(1, "Test Product", "Test Product Description", new Date());
    product.category = new Category(1, "Test Category", "Test Category Description");
    this.products.push(product);
    this.products$ = new BehaviorSubject<Product[]>(this.products);
  }

  all(): Observable<Product[]> {
    return this.products$.asObservable();
  }

  allByCategoryId(categoryId: number): Observable<Product[]>{
    return this.products$.pipe(map(data => data.filter(product => product.category.id == categoryId)));
  }

  byId(productId: number): Observable<Product> {
    var product = this.products.find(c => c.id == productId);
    return Observable.create(observer => {
      observer.next(product);
    });
  }

  add(product: Product): Observable<number> {
    if (this.products === undefined || this.products.length === 0) {
      product.id = 1;
    }
    else {
      product.id = this.products[this.products.length - 1].id + 1;
    }
    this.products.push(product);
    this.products$.next(this.products);

    return Observable.create(observer => {
      observer.next(product.id);
    });
  }

  delete(productId: number): Observable<boolean> {
    let result: boolean = true;
    var dbProduct = this.products.find(c => c.id == productId);
    if (dbProduct === undefined || dbProduct === null) {
      result = false;
    }
    else {
      this.products.splice(this.products.indexOf(dbProduct), 1);
    }
    this.products$.next(this.products);
    return Observable.create(observer => {
      observer.next(result);
      observer.complete;
    });
  }


  update(product: Product): Observable<boolean> {
    let result: boolean = true;
    var dbProduct = this.products.find(c => c.id == product.id);
    if (dbProduct === undefined || dbProduct === null) {
      result = false;
    }
    else {
      dbProduct.description = product.description;
      dbProduct.name = product.name;
    }
    this.products$.next(this.products);
    return Observable.create(observer => {
      observer.next(result);
      observer.complete;
    });
  }
}
