import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDeleteComponent } from './confirm-delete/confirm-delete.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UpdateEntityComponent } from './update-entity/update-entity.component'
import { FormsModule } from '@angular/forms';
import { PagerComponent } from './pager/pager.component';
@NgModule({
  declarations: [ConfirmDeleteComponent, UpdateEntityComponent, PagerComponent],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule
  ],
  exports: [CommonModule, NgbModule, ConfirmDeleteComponent, FormsModule, PagerComponent],
  bootstrap: [ConfirmDeleteComponent, UpdateEntityComponent]
})
export class SharedModule { }
