import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateEntityComponent } from './update-entity.component';
import { NgForm, NgModel } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

describe('UpdateEntityComponent', () => {
  let component: UpdateEntityComponent;
  let fixture: ComponentFixture<UpdateEntityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers:[NgbActiveModal],
      declarations: [ UpdateEntityComponent,NgForm, NgModel ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateEntityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
