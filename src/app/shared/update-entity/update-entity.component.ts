import { Component, ChangeDetectionStrategy, Input, Output } from '@angular/core';
import {  NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-update-entity',
  templateUrl: './update-entity.component.html',
  styleUrls: ['./update-entity.component.css']
})
export class UpdateEntityComponent  {
  @Input() entity:string;
  @Input() entityType: string;
  @Input() type: string;
  constructor(private modalService:NgbActiveModal) { }
  save(){
    this.modalService.close(this.entity);
  }
  close(){
    this.modalService.close();
  }
  dismiss(){
    this.modalService.dismiss();
  }

}
