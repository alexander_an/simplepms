import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { PagingServiceService } from '../services/paging-service.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-pager',
  templateUrl: './pager.component.html',
  styleUrls: ['./pager.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PagerComponent implements OnInit {
  @Input() pageSize: number;
  @Input() items: Observable<any>;
  data: any[] = [];
  @Output() pageItems: EventEmitter<any> = new EventEmitter();
  pager: any = {};
  private currentPage: number = 1;
  constructor(private pagingService: PagingServiceService, private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.items.subscribe(items => {
      this.data = items;
      this.cdr.markForCheck();
      this.setCurrentPage(this.data);
      this.setPage(this.currentPage);
    })

  }

  setCurrentPage(items: any[]) {
    if(this.pager.totalPages == 0)
      this.pager.totalPages = 1;
      this.currentPage = 1;
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagingService.getPager(this.data.length, page, this.pageSize);
    this.currentPage = this.pager.currentPage;
    this.pageItems.emit(this.data.slice(this.pager.startIndex, this.pager.endIndex + 1));
    this.cdr.detectChanges();
  }
}
