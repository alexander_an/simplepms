import { Category } from './category';

export class Product {
    public id:number;
    public name: string;
    public description: string;
    public createdOn: Date;
    public category: Category;
    /**
     *@param id
     */
    constructor(id:number, name:string, description:string, createdOn:Date) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.createdOn = createdOn;
    }
}
